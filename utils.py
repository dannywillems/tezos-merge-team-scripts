class Wrapper:
    def __init__(self, wrapped):
        self.wrapped = wrapped

    def __getattr__(self, attr):
        try:
            return getattr(self.wrapped, attr)
        except AttributeError as error:
            if attr == 'wrapped_more':
                raise error
            more = getattr(self, 'wrapped_more', None)
            if more is None:
                raise error
            else:
                self.wrapped = more
                return getattr(more, attr)
