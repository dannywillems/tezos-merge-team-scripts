from abc import ABC
from functools import cached_property
from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    List,
    Literal,
    NamedTuple,
    Optional,
    Sequence,
    Set,
    Tuple,
    Type,
    TypeVar,
    Union,
)
from datetime import datetime, timedelta
from gitlab import Gitlab  # type: ignore
from utils import Wrapper
import re
import unicodedata
import os
import gitlab.v4.objects as gitlab_objects  # type: ignore
import projects_spec

# Parsers and textual heuristics

ResourceKind = Literal['!', '#']
Dependency = Tuple[str, ResourceKind, int]

RE_MENTION = re.compile(r'@(\w(?:[-.\w]|\\[-._])*)')
RE_PUSH = re.compile(r'added \d+ commit')
RE_BLOCKERS = re.compile(
    r'^(?:(?:Blocked|Depend)\w*\b(.*))$', re.MULTILINE + re.IGNORECASE
)
RE_DEPENDENCY = re.compile(
    r'(?P<uri>(?:https?://)?(?:www\.)?gitlab\.com/)?((?(uri)\w[-\w/]*|(?:\b\w[-\w]*/\w[-\w/]*)?))((?(uri)(?:issues|merge_requests)|[!#]))(?(uri)/)(\d+)(?:\b|(?(uri)#\w*|\b))'
)
RE_ISSUE = re.compile(r'^([^#]+)#(\d+)$')
RE_WIP = re.compile(r'^WIP\b', re.IGNORECASE)
RE_META = re.compile(
    r'^(?:\[?(?:meta[- ]issue\b|overview\b|☂))|(?:.*\bmeta[- ]issue$)',
    re.IGNORECASE,
)
RE_NON_FILENAME = re.compile(r'[^\w.-]')
RE_NON_FILEPATH = re.compile(f'[^\w.{re.escape(os.sep)}-]')


def is_automatic_merge(text: str) -> bool:
    return text.startswith('enabled an automatic merge')


def get_blockers_sections(text: str) -> List[str]:
    return RE_BLOCKERS.findall(text)


def parse_dependency(
    raw_dep: Tuple[
        str, str, Literal['!', '#', 'issues', 'merge_requests'], str
    ]
) -> Dependency:
    server, repo, kind, id = raw_dep
    if repo.endswith('/-/'):
        repo = repo[:-3]
    elif repo.endswith('/'):
        repo = repo[:-1]
    if kind == 'issues':
        kind = '#'
    elif kind == 'merge_requests':
        kind = '!'
    return (repo, kind, int(id))


def get_dependencies(text: str) -> List[Dependency]:
    return [parse_dependency(d) for d in RE_DEPENDENCY.findall(text)]


def is_changed_line(text: str) -> bool:
    return text.startswith('changed this line')


def is_push(text: str) -> bool:
    return RE_PUSH.match(text) is not None


def normalize_mention(name: str) -> str:
    return name.replace('\\', '')


def get_mentioned_users(text: str) -> Set[str]:
    return set(normalize_mention(u) for u in RE_MENTION.findall(text))


def get_reviewers_section(text: str) -> str:
    i = text.lower().rfind('reviewer')
    return text[i:] if i >= 0 else ''


def parse_datetime(text: str) -> datetime:
    if text.endswith('Z'):
        text = text[:-1]
    return datetime.fromisoformat(text)


def parse_issue_ref(ref: str) -> Tuple[str, int]:
    m = RE_ISSUE.match(ref)
    assert m is not None
    repo, id = m.groups()
    return (repo, int(id))


def title_is_wip(title: str) -> bool:
    return RE_WIP.match(title) is not None


def title_is_meta(title: str) -> bool:
    return RE_META.match(title) is not None


def to_ascii(s: str) -> str:
    return (
        unicodedata.normalize('NFKD', s)
        .encode('ascii', 'ignore')
        .decode('ascii')
    )


def to_filename(s: str) -> str:
    return RE_NON_FILENAME.sub('*', to_ascii(s)).strip('*').replace('*', '_')


def to_filepath(s: str) -> str:
    return RE_NON_FILEPATH.sub('*', to_ascii(s)).strip('*').replace('*', '_')


# Classes


class User:
    avatar_url = None
    weight = None

    def __init__(
        self, server: 'Server', username: str,
    ):
        self.username = username
        self.name = username
        self.web_url = f'https://gitlab.com/{username}'
        self.server = server

    def update_from_dict(self, dic: dict) -> 'User':
        self.name = dic['name']
        self.avatar_url = dic['avatar_url']
        self.web_url = dic['web_url']
        return self

    def update_from_project_member(
        self, mem: gitlab_objects.ProjectMember
    ) -> 'User':
        self.name = mem.name
        self.avatar_url = mem.avatar_url
        self.web_url = mem.web_url
        return self

    def __lt__(self, other: 'User') -> bool:
        return self.username < other.username

    def __eq__(self, other: object) -> bool:
        return isinstance(other, User) and self.username == other.username

    def __hash__(self) -> int:
        return hash(self.username)

    @cached_property
    def printable(self) -> str:
        return self.username

    @cached_property
    def printable_name(self) -> str:
        return getattr(self, 'name', self.printable)

    @cached_property
    def avatar_alt(self) -> str:
        return f'@{self.username}'

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.username.__repr__()})'

    @cached_property
    def is_dashboard_maintainer(self) -> bool:
        return self in self.server.dashboard_maintainers

    @cached_property
    def filename(self) -> str:
        return to_filename(self.username)


TTempUser = TypeVar('TTempUser', bound='TempUser')


class TempUser(User):
    def __init__(self, username: str):
        self.username = username

    @classmethod
    def from_dict(cls: Type[TTempUser], dic: dict) -> TTempUser:
        return cls(dic['username'])


class Push(Wrapper):
    def __init__(self, wrapped: Union['Comment', 'MergeRequest']):
        super().__init__(wrapped)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.wrapped.__repr__()})'


class Comment:
    def __init__(self, thread: 'Thread', note_dict: dict):
        self.thread = thread
        self.__attributes = note_dict

    @cached_property
    def uid(self) -> int:
        return self.__attributes['id']

    @cached_property
    def author(self) -> User:
        return self.thread.mr.repo.server.user_from_dict(
            self.__attributes['author']
        )

    @cached_property
    def system(self) -> bool:
        return self.__attributes['system']

    @cached_property
    def body(self) -> str:
        return self.__attributes['body']

    @cached_property
    def created_at(self) -> datetime:
        return parse_datetime(self.__attributes['created_at'])

    @cached_property
    def changed_line(self) -> bool:
        return self.system and is_changed_line(self.body)

    @cached_property
    def automatic_merger(self) -> Optional[User]:
        return (
            self.author
            if self.system and is_automatic_merge(self.body)
            else None
        )

    @cached_property
    def as_push(self) -> Optional[Push]:
        return Push(self) if self.system and is_push(self.body) else None

    @cached_property
    def web_url(self) -> str:
        return f"{self.thread.mr.web_url}#note_{self.__attributes['id']}"

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.uid.__repr__()} on {self.thread.__repr__()})'

    @cached_property
    def weight(self) -> Tuple[float, Any]:
        return (-self.created_at.timestamp(), self.thread.weight)


class Thread(Wrapper):
    def __init__(
        self,
        mr: 'MergeRequest',
        discussion: gitlab_objects.ProjectMergeRequestDiscussion,
    ):
        super().__init__(discussion)
        self.mr = mr
        self.__notes = discussion.attributes['notes']

    @cached_property
    def unresolved_notes(self) -> List[dict]:
        return [
            n
            for n in self.__notes[::-1]
            if n['resolvable'] and not n['resolved']
        ]

    @cached_property
    def last_non_author_unresolved_comment(self) -> Optional[Comment]:
        for n in self.unresolved_notes:
            if TempUser.from_dict(n['author']) not in self.mr.authors:
                return Comment(self, n)
        return None

    @cached_property
    def unresolved_comments(self) -> Dict[User, Comment]:
        return {
            self.mr.repo.server.user_from_dict(n['author']): Comment(self, n)
            for n in self.unresolved_notes[::-1]
        }

    @cached_property
    def resolved(self) -> bool:
        return not any(self.unresolved_notes)

    @cached_property
    def last_comment(self) -> Comment:
        return Comment(self, self.__notes[-1])

    @cached_property
    def last_comment_is_from_author(self) -> bool:
        return self.last_comment.author in self.mr.authors

    @cached_property
    def line_has_been_updated(self) -> bool:
        return self.last_comment.changed_line

    @cached_property
    def last_comment_is_from_author_or_line_has_been_updated(self) -> bool:
        return self.last_comment_is_from_author or self.line_has_been_updated

    @cached_property
    def automatic_merger(self) -> Optional[User]:
        return (
            self.last_comment.automatic_merger
            if self.wrapped.individual_note
            else None
        )

    @cached_property
    def as_push(self) -> Optional[Push]:
        return (
            self.last_comment.as_push if self.wrapped.individual_note else None
        )

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.wrapped.id.__repr__()} on {self.mr.__repr__()})'

    @cached_property
    def weight(self) -> int:
        return len(self.__notes)


class Label(ABC):
    weight: int

    def __init__(
        self, repo: 'Repository', name: str,
    ):
        self.repo = repo
        self.name = name

    @cached_property
    def filename(self) -> str:
        return to_filename(self.name)

    @cached_property
    def is_meta(self) -> bool:
        return self.name in ['Meta', 'meta', 'meta-issue']


class UnknownLabel(Label):
    color = 'white'
    text_color = 'black'
    weight = -2
    description = ''


class ProjectLabel(Wrapper, Label):
    def __init__(self, repo: 'Repository', label: gitlab_objects.ProjectLabel):
        Wrapper.__init__(self, label)
        Label.__init__(self, repo, label.name)
        self.color = label.color
        self.text_color = label.text_color
        self.weight = label.priority if label.priority is not None else (-1)
        self.description = (
            label.description if label.description is not None else ''
        )


class Resource(Wrapper):
    def __init__(self, repo: 'Repository', resource):
        super().__init__(resource)
        self.repo = repo
        self.blocking: Set['Resource'] = set()

    @cached_property
    def uid(self) -> str:
        return self.wrapped.id

    @cached_property
    def printable_id(self) -> str:
        return self.wrapped.references['full']

    def __lt__(self, other: 'Resource') -> bool:
        return self.printable_id < other.printable_id

    def __eq__(self, other: object) -> bool:
        return (
            isinstance(other, Resource)
            and self.printable_id == other.printable_id
        )

    def __hash__(self) -> int:
        return hash(self.uid)

    @cached_property
    def title(self) -> str:
        return self.wrapped.title if self.visible else self.printable_id

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.printable_id.__repr__()})'

    @cached_property
    def created_at(self) -> datetime:
        return parse_datetime(self.wrapped.attributes['created_at'])

    def mark_as_blocking(self, other: 'Resource') -> None:
        self.blocking.add(other)

    @cached_property
    def description(self) -> str:
        return (
            self.wrapped.description
            if self.wrapped.description is not None
            else ''
        )

    @cached_property
    def blockers(self) -> Set['Resource']:
        # TODO: currently no API to get dependent MRs/issues
        res = {
            d
            for d in (
                self.repo.get_open_dependency(dep)
                for section in get_blockers_sections(self.description)
                for dep in (get_dependencies(section))
            )
            if d is not None
        }
        for b in res:
            b.mark_as_blocking(self)
        return res

    @cached_property
    def blocked(self) -> bool:
        return any(self.blockers)

    @cached_property
    def printable_blockers(self) -> str:
        return ', '.join([blocker.printable_id for blocker in self.blockers])

    @cached_property
    def pre_weight(self) -> Any:
        return [len(self.blocking), self.repo.pre_weight]

    @cached_property
    def post_weight(self) -> Any:
        return (
            self.upvotes - self.downvotes,
            self.labels_weights,
            -self.created_at.timestamp(),
        )

    @cached_property
    def labels(self) -> Set[Label]:
        return set(self.repo.get_label(l) for l in self.wrapped.labels)

    @cached_property
    def labels_weights(self) -> List[int]:
        return sorted((l.weight for l in self.labels), reverse=True)[:3]


class MergeRequest(Resource):
    def __init__(
        self,
        repo: 'Repository',
        mr: Union[
            gitlab_objects.MergeRequest, gitlab_objects.ProjectMergeRequest
        ],
    ):
        super().__init__(repo, mr)

    @cached_property
    def wrapped_more(self) -> gitlab_objects.MergeRequest:
        return self.repo.mergerequests.get(
            self.wrapped.iid,
            include_diverged_commits_count=True,
            include_rebase_in_progress=True,
        )

    @cached_property
    def author(self) -> User:
        return self.repo.server.user_from_dict(self.wrapped.author)

    @cached_property
    def pushes(self) -> Sequence[Push]:
        first_push = Push(self)
        thread_pushes = [
            t.as_push for t in self.threads if t.as_push is not None
        ]
        return [first_push] + thread_pushes

    @cached_property
    def last_push(self) -> datetime:
        if any(self.pushes):
            return self.pushes[-1].created_at
        return parse_datetime(self.wrapped.created_at)

    @cached_property
    def authors(self) -> Dict[User, str]:
        last_author_date = self.last_push - timedelta(days=60)
        authors = {
            p.author: 'pushed recently'
            for p in self.pushes
            if p.created_at < last_author_date
        }
        for p in self.pushes[-5:]:
            if p.author not in authors:
                authors[p.author] = 'pushed recently'
        if self.author in authors or not any(authors):
            authors[self.author] = 'is the original author'
        return authors

    @cached_property
    def merger(self) -> Optional[Tuple[User, str]]:
        if self.wrapped.merged_by is not None:
            return (
                self.repo.server.user_from_dict(self.wrapped.merged_by),
                'merged',
            )
        for t in self.threads[::-1]:
            if t.automatic_merger:
                return (t.automatic_merger, 'set automatic merge')
        return None

    @cached_property
    def authors_and_merger(self) -> Dict[User, str]:
        res = self.authors
        if not self.merger:
            return res
        merger_user, merger_why = self.merger
        if merger_user in res:
            merger_why = f'{res[merger_user]} and {merger_why}'
        return {**res, merger_user: merger_why}

    @cached_property
    def authors_and_pipeline_user(self) -> Dict[User, str]:
        res = self.authors
        head_pipeline_user = self.repo.server.user_from_dict(
            self.head_pipeline['user']
        )
        head_pipeline_why = 'triggered last pipeline'
        if head_pipeline_user in res:
            head_pipeline_why = (
                f'{res[head_pipeline_user]} and {head_pipeline_why}'
            )
        return {**res, head_pipeline_user: head_pipeline_why}

    @cached_property
    def reviewers_in_description(self) -> Set[User]:
        return {
            self.repo.server.user_from_username(un)
            for un in get_mentioned_users(
                get_reviewers_section(self.description)
            )
        }

    @cached_property
    def assignees(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(u) for u in self.wrapped.assignees
        }

    @cached_property
    def approvals(self) -> gitlab_objects.ProjectMergeRequestApproval:
        return self.wrapped.approvals.get()

    @cached_property
    def approvers(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(a['user'])
            for a in self.approvals.approved_by
        }

    @cached_property
    def set_reviewers(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(u) for u in self.wrapped.reviewers
        }

    @cached_property
    def all_reviewers(self) -> Set[User]:
        return (
            self.set_reviewers
            | self.reviewers_in_description
            | self.assignees
            | self.approvers
        )

    @cached_property
    def threads(self) -> Sequence[Thread]:
        return [
            Thread(self, d) for d in self.wrapped.discussions.list(all=True)
        ]

    @cached_property
    def unresolved_threads(self) -> List[Thread]:
        return [t for t in self.threads if not t.resolved]

    @cached_property
    def has_unresolved_threads(self) -> bool:
        return any(self.unresolved_threads)

    @cached_property
    def one_approval_left(self) -> bool:
        return self.approvals.approvals_left == 1

    @cached_property
    def has_at_least_one_approval(self) -> bool:
        return any(self.approvals.approved_by)

    @cached_property
    def has_uncompleted_tasks(self) -> bool:
        tcs = self.wrapped.task_completion_status
        return tcs['completed_count'] < tcs['count']

    @cached_property
    def task_completion_uid(self) -> str:
        tcs = self.wrapped.task_completion_status
        return f"{tcs['completed_count']}/{tcs['count']}"

    @cached_property
    def task_completion_weight(self) -> int:
        tcs = self.wrapped.task_completion_status
        return tcs['count'] - tcs['completed_count']

    @cached_property
    def has_diverged(self) -> bool:
        return self.diverged_commits_count > 0

    @cached_property
    def non_approvers_reviewers(self) -> Dict[User, str]:
        return {
            **{
                u: 'is mentioned as reviewer'
                for u in self.reviewers_in_description - self.approvers
            },
            **{u: 'is assigned' for u in self.assignees - self.approvers},
            **{
                u: 'is set as reviewer'
                for u in self.set_reviewers - self.approvers
            },
        }

    @cached_property
    def mergeteam_reviewers(self) -> Dict[User, str]:
        return {
            u: f'is a merge-teamer and {w}'
            for u, w in self.non_approvers_reviewers.items()
            if u in self.repo.mergeteamers
        }

    @cached_property
    def mergers(self) -> Dict[User, str]:
        return (
            self.mergeteam_reviewers
            if self.mergeteam_reviewers
            else {
                u: f'is a dispatcher and there is no merge-team reviewer'
                for u in self.repo.dispatchers
            }
        )

    @cached_property
    def has_less_than_2_reviewers(self) -> bool:
        return len(self.all_reviewers) < 2

    @cached_property
    def has_no_non_dispatcher_mergeteamers_reviewers(self) -> bool:
        return not any(self.mergeteam_reviewers.keys() - self.repo.dispatchers)

    @cached_property
    def last_pipeline_status(self) -> Optional[str]:
        return (
            self.head_pipeline['status']
            if self.head_pipeline is not None
            else None
        )

    @cached_property
    def last_pipeline_failed(self) -> bool:
        return self.last_pipeline_status == 'failed'

    @cached_property
    def last_pipeline_succeeded(self) -> bool:
        return (
            self.last_pipeline_status is None
            or self.last_pipeline_status == 'success'
        )

    @cached_property
    def pipelines(self) -> Iterable[dict]:
        return self.wrapped.pipelines()

    @cached_property
    def no_pipeline_finished(self) -> bool:
        for pipeline in self.pipelines:
            if pipeline['status'] != 'running':
                return False
        return True

    @cached_property
    def last_finished_pipeline_succeeded(self) -> bool:
        if self.last_pipeline_succeeded:
            return True
        for pipeline in self.pipelines:
            if pipeline['status'] == 'success':
                return True
            if pipeline['status'] == 'failed':
                return False
        return False

    @cached_property
    def base_uid(self) -> str:
        return self.diff_refs['base_sha']

    @cached_property
    def wip(self) -> bool:
        return self.work_in_progress or title_is_wip(self.wrapped.title)

    @cached_property
    def post_weight(self) -> Any:
        return (self.first_contribution, super().post_weight)

    @cached_property
    def visible(self) -> bool:
        return self.repo.merge_requests_visible


class Issue(Resource):
    is_special = False

    def __init__(self, repo: 'Repository', issue: gitlab_objects.ProjectIssue):
        super().__init__(repo, issue)

    @cached_property
    def author(self) -> User:
        return self.repo.server.user_from_dict(self.wrapped.author)

    @cached_property
    def assignees(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(u) for u in self.wrapped.assignees
        }

    @cached_property
    def is_assigned(self) -> bool:
        return any(self.assignees)

    @cached_property
    def owners(self) -> Dict[User, str]:
        return (
            {u: 'is assigned' for u in self.assignees}
            if any(self.assignees)
            else {self.author: 'opened issue and no one is assigned'}
        )

    @cached_property
    def visible(self) -> bool:
        return self.repo.server.allow_private or (
            self.repo.issues_visible and not self.confidential
        )

    @cached_property
    def is_meta(self) -> bool:
        for l in self.labels:
            if l.is_meta:
                return True
        return title_is_meta(self.wrapped.title)


class Repository(Wrapper):
    def __init__(
        self,
        server: 'Server',
        project: Union[gitlab_objects.Project, gitlab_objects.GroupProject],
    ):
        super().__init__(project)
        self.server = server
        self.registered = False
        self.project_people_spec = projects_spec.EMPTY_PROJECT_PEOPLE_SPEC
        self.__issues: Dict[int, Issue] = dict()
        self.__labels: Dict[str, Label] = dict()

    @cached_property
    def wrapped_more(self) -> gitlab_objects.Project:
        return self.server.fetch_raw_project(self.wrapped.id)

    @cached_property
    def owners(self) -> Set[User]:
        members = list(self.members.all(all=True))
        highest_level = max(members, key=lambda m: m.access_level).access_level
        return {
            self.server.user_from_project_member(m)
            for m in members
            if m.access_level == highest_level
        }

    @cached_property
    def dispatchers_and_why(self) -> Tuple[Set[User], str]:
        return self.server.users_from_people_spec(
            self.project_people_spec.dispatchers,
            repo=self,
            default=(self.owners, 'from project owners'),
        )

    @cached_property
    def dispatchers(self) -> Set[User]:
        return self.dispatchers_and_why[0]

    @cached_property
    def dispatchers_why(self) -> str:
        return self.dispatchers_and_why[1]

    @cached_property
    def dashboard_maintainers_or_dispatchers(self) -> Set[User]:
        return (
            self.server.dashboard_maintainers
            if any(self.server.dashboard_maintainers)
            else self.dispatchers
        )

    @cached_property
    def issuewatchers_and_why(self) -> Tuple[Set[User], str]:
        return self.server.users_from_people_spec(
            self.project_people_spec.issuewatchers,
            repo=self,
            default=(set(), 'no one'),
        )

    @cached_property
    def issuewatchers(self) -> Set[User]:
        return self.issuewatchers_and_why[0]

    @cached_property
    def issuewatchers_why(self) -> str:
        return self.issuewatchers_and_why[1]

    @cached_property
    def required_approvers(self) -> Set[User]:
        return (
            {
                self.server.user_from_dict(user)
                for rule in self.approvalrules.list(all=True)
                if rule.approvals_required > 0
                for user in rule.eligible_approvers
            }
            if self.merge_requests_enabled
            else set()
        )

    @cached_property
    def mergeteamers_and_why(self) -> Tuple[Set[User], str]:
        return self.server.users_from_people_spec(
            self.project_people_spec.mergeteamers,
            repo=self,
            default=(self.required_approvers, 'from approval rules')
            if any(self.required_approvers)
            else (self.dispatchers, 'copied from dispatchers'),
        )

    @cached_property
    def mergeteamers(self) -> Set[User]:
        return self.mergeteamers_and_why[0]

    @cached_property
    def mergeteamers_why(self) -> str:
        return self.mergeteamers_and_why[1]

    @cached_property
    def printable_dispatchers(self) -> str:
        return ', '.join(sorted([d.username for d in self.dispatchers]))

    @cached_property
    def opened_mrs(self) -> List[MergeRequest]:
        return (
            [
                MergeRequest(self, mr)
                for mr in self.mergerequests.list(state='opened', all=True)
            ]
            if self.wrapped.merge_requests_enabled
            else []
        )

    def get_issue(self, iid: int) -> Issue:
        res = self.__issues.get(iid)
        if res is None:
            res = self.__add_raw_issue(self.issues.get(iid))
        return res

    def __add_raw_issue(self, raw_issue: gitlab_objects.Issue) -> Issue:
        res = Issue(self, raw_issue)
        self.__issues[res.iid] = res
        return res

    @cached_property
    def opened_issues(self) -> List[Issue]:
        return (
            [
                self.__add_raw_issue(issue)
                for issue in self.issues.list(state='opened', all=True)
            ]
            if self.wrapped.issues_enabled
            else []
        )

    def get_label(self, name: str) -> Label:
        _ = self.__load_labels
        res = self.__labels.get(name)
        if res is None:
            res = UnknownLabel(self, name)
            self.__labels[name] = res
        return res

    @cached_property
    def __load_labels(self) -> None:
        for l in self.wrapped.labels.list(all=True):
            self.__labels[l.name] = ProjectLabel(self, l)

    @cached_property
    def labels(self) -> Set[Label]:
        _ = self.__load_labels
        return set(self.__labels.values())

    @cached_property
    def public_id(self) -> str:
        return self.wrapped.path_with_namespace

    @cached_property
    def printable_id(self) -> str:
        # Replace hyphen by non-breakable hyphen to avoid breaking lines
        return self.public_id.replace('-', '‑')

    @cached_property
    def visible(self) -> bool:
        return self.server.allow_private or self.wrapped.visibility == 'public'

    @cached_property
    def issues_visible(self) -> bool:
        return self.server.allow_private or (
            self.visible and self.issues_access_level == 'enabled'
        )

    @cached_property
    def merge_requests_visible(self) -> bool:
        return self.server.allow_private or (
            self.visible and self.merge_requests_access_level == 'enabled'
        )

    def register_people(self, spec: projects_spec.ProjectPeopleSpec) -> None:
        self.registered = True
        self.project_people_spec = spec.inherited_from(
            self.project_people_spec
        )

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.public_id.__repr__()})'

    @cached_property
    def avatar_alt(self) -> str:
        return self.public_id

    @cached_property
    def pre_weight(self) -> int:
        return (
            self.project_people_spec.pre_weight
            if self.project_people_spec.pre_weight is not None
            else -1
        )

    @cached_property
    def filepath(self) -> str:
        return to_filepath(self.public_id)

    def get_open_resource(
        self, kind: ResourceKind, id: int
    ) -> Optional[Resource]:
        if kind == '!':
            return next((mr for mr in self.opened_mrs if mr.iid == id), None)
        else:
            return next(
                (issue for issue in self.opened_issues if issue.iid == id),
                None,
            )

    def get_open_dependency(self, dep_ref: Dependency) -> Optional[Resource]:
        if dep_ref[0] == '' or dep_ref[0] == self.public_id:
            repo = self
        else:
            repo = self.server.get_repo(dep_ref[0])
        return repo.get_open_resource(dep_ref[1], dep_ref[2])


class Group(Wrapper):
    def __init__(
        self,
        server: 'Server',
        group: Union[gitlab_objects.Group, gitlab_objects.GroupSubgroup],
    ):
        super().__init__(group)
        self.server = server

    @cached_property
    def repos(self) -> List[Repository]:
        return [
            self.server.get_or_add_raw_repo(p)
            for p in self.wrapped.projects.list(all=True)
        ]

    def register_people(self, spec: projects_spec.ProjectPeopleSpec) -> None:
        for p in self.repos:
            p.register_people(spec)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.full_path.__repr__()})'


TServer = TypeVar('TServer', bound='Server')


class Server(Wrapper):
    def __init__(
        self,
        gitlab: Gitlab,
        allow_private: bool = False,
        dashboard_maintainers_spec: projects_spec.PeopleSpec = projects_spec.NO_ONE,
    ):
        super().__init__(gitlab)
        self.__group_by_name: Dict[str, Group] = dict()
        self.__group_by_id: Dict[int, Group] = dict()
        self.__repo_by_name: Dict[str, Repository] = dict()
        self.__repo_by_id: Dict[int, Repository] = dict()
        self.users: Dict[str, User] = dict()
        self.allow_private = allow_private
        self.dashboard_maintainers_spec = dashboard_maintainers_spec

    @classmethod
    def default(
        cls: Type[TServer],
        *,
        projects: projects_spec.ProjectRef = projects_spec.EMPTY_PROJECT_SPEC,
        private_token: Optional[str] = None,
        allow_private: bool = False,
        dashboard_maintainers: projects_spec.PeopleSpec = projects_spec.NO_ONE,
    ) -> TServer:
        server = cls(
            Gitlab('https://www.gitlab.com', private_token=private_token),
            allow_private,
            dashboard_maintainers_spec=dashboard_maintainers,
        )
        if private_token is not None:
            server.auth()
        server.register_project_spec(projects)
        return server

    @cached_property
    def registered_repositories(self) -> List[Repository]:
        return [
            repo
            for repo in self.__repo_by_id.values()
            if repo.registered and repo.visible
        ]

    def register_project_spec(self, spec: projects_spec.ProjectRef) -> None:
        if isinstance(spec, projects_spec.Project):
            repo = self.get_repo(spec)
            repo.register_people(spec)
        elif isinstance(spec, projects_spec.Group):
            group = self.get_group(spec)
            group.register_people(spec)
        elif isinstance(spec, projects_spec.List):
            for i in spec.items:
                self.register_project_spec(i.inherited_from(spec))
        else:
            raise ValueError(
                f'Unknown project spec kind {type(projects_spec)}'
            )

    def fetch_raw_project(
        self, id: projects_spec.Id
    ) -> gitlab_objects.Project:
        print(f'Fetching project {id}', flush=True)
        return self.wrapped.projects.get(id)

    def fetch_raw_group(self, id: projects_spec.Id) -> gitlab_objects.Group:
        print(f'Fetching group {id}', flush=True)
        return self.wrapped.groups.get(id)

    def get_repo(self, id: projects_spec.RefId) -> Repository:
        if isinstance(id, projects_spec.Ref):
            return self.get_repo(id.id)
        if isinstance(id, int):
            res = self.__repo_by_id.get(id)
        elif isinstance(id, str):
            res = self.__repo_by_name.get(id)
        else:
            raise ValueError(f'Unknown project ref kind {type(id)}')
        if res is None:
            res = self.__add_new_repo(self.fetch_raw_project(id))
        return res

    def get_group(self, id: projects_spec.RefId) -> Group:
        if isinstance(id, projects_spec.Ref):
            return self.get_group(id.id)
        if isinstance(id, int):
            res = self.__group_by_id.get(id)
        elif isinstance(id, str):
            res = self.__group_by_name.get(id)
        else:
            raise ValueError(f'Unknown group ref kind {type(id)}')
        if res is None:
            res = self.__add_new_group(self.fetch_raw_group(id))
        return res

    def __add_new_repo(
        self,
        gitlab_project: Union[
            gitlab_objects.Project, gitlab_objects.GroupProject
        ],
    ) -> Repository:
        res = Repository(self, gitlab_project)
        self.__repo_by_id[res.id] = res
        self.__repo_by_name[res.path_with_namespace] = res
        return res

    def get_or_add_raw_repo(
        self,
        gitlab_project: Union[
            gitlab_objects.Project, gitlab_objects.GroupProject
        ],
    ) -> Repository:
        res = self.__repo_by_id.get(gitlab_project.id)
        if res is None:
            res = self.__add_new_repo(gitlab_project)
        return res

    def __add_new_group(self, gitlab_group: gitlab_objects.Group) -> Group:
        res = Group(self, gitlab_group)
        self.__group_by_id[res.id] = res
        self.__group_by_name[res.full_path] = res
        return res

    def users_from_people_spec(
        self,
        spec: Optional[projects_spec.PeopleSpec],
        *,
        default: Union[
            Tuple[Set[User], str], Callable[[], Tuple[Set[User], str]]
        ],
        repo: Optional[Repository] = None,
    ) -> Tuple[Set[User], str]:
        if spec is None or isinstance(spec, projects_spec.Default):
            if callable(default):
                return default()
            else:
                return default
        elif isinstance(spec, projects_spec.Users):
            return (
                {
                    self.user_from_username(username)
                    for username in spec.usernames
                },
                'from hardcoded list',
            )
        elif isinstance(spec, projects_spec.Issue):
            issue = self.get_issue(spec, repo=repo)
            issue.is_special = True
            return (issue.assignees, f'from issue {issue.printable_id}')
        else:
            raise ValueError(f'Unknown people spec kind {type(spec)}')

    def get_issue(
        self, id: projects_spec.RefId, *, repo: Optional[Repository] = None
    ) -> Issue:
        if isinstance(id, projects_spec.Ref):
            return self.get_issue(id.id, repo=repo)
        if isinstance(id, int):
            if repo is None:
                raise ValueError(
                    f'Cannot get issue {id} with no specified repository'
                )
            else:
                return repo.get_issue(id)
        elif isinstance(id, str):
            reponame, issue_id = parse_issue_ref(id)
            return self.get_repo(reponame).get_issue(issue_id)
        else:
            raise ValueError(f'Unknown issue reference kind {type(id)}')

    def user_from_username(self, username: str) -> User:
        res = self.users.get(username)
        if res is None:
            res = User(self, username)
            self.users[username] = res
        return res

    def user_from_dict(self, dic: dict) -> User:
        return self.user_from_username(dic['username']).update_from_dict(dic)

    def user_from_project_member(
        self, mem: gitlab_objects.ProjectMember
    ) -> User:
        return self.user_from_username(
            mem.username
        ).update_from_project_member(mem)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.wrapped.__repr__()})'

    @cached_property
    def dashboard_maintainers(self) -> Set[User]:
        return self.users_from_people_spec(
            self.dashboard_maintainers_spec, default=(set(), '')
        )[0]
